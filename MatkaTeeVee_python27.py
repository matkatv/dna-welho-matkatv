#!/usr/bin/env python
# -*- coding: iso-8859-15 -*-

import os, sys, subprocess, re
from PyQt4 import QtCore, QtGui, QtWebKit, QtNetwork

#K�ytt�:
#Lataus- ja toistotilan v�lill� vaihdetaan ctrl:lla.
#Alle t�ytyy lis�t� k�ytt�j�tunnus ja salasana jos haluaa kirjautua automaattisesti ja
#komennot VLC:n ja Wgetin k�ynnist�miseen. Valittu toiminto k�ynnistyy tallenteen play kuvakkeesta.
#Vaatii toimiakseen python 2.7, PyQt4, VLC:n ja Wgetin.

#k�ytt�j�tunnus ja salasana automaattiselle kirjautumiselle
uname  = ""
passw  = ""

#komennot lataamiselle ja toistolle
vlc       = "vlc --network-caching=5000 --http-reconnect "
wget      = "wget -t0 -c -S --limit-rate=200k "

#jos haluat avata uudet lataukset omiin ikkunoihinsa niin poista kommentointi
#wget	  = "gnome-terminal -e '" + wget

#kansio jonne haluaa tallennusten menev�n
directory = "MatkaTV/"

if directory != '':
	if os.path.isfile(directory) == False:
		if os.path.isdir(directory) == False:
			os.makedirs(directory)

class MatkaTeeve(QtGui.QWidget):

	def __init__(self, parent = None):
		QtGui.QWidget.__init__(self, parent)
		self.resize(1200, 700)
		screen = QtGui.QDesktopWidget().screenGeometry()
		self.setWindowTitle("MatkaTeevee - Toisto")
		self.web = QtWebKit.QWebView(self)
		self.web.settings().setAttribute(QtWebKit.QWebSettings.PluginsEnabled,True)
		self.web.setZoomFactor(0.8)
		layout = QtGui.QVBoxLayout(self)
		layout.setMargin(0)
		layout.addWidget(self.web)
		self.dl = False
		self.programPage =""
		self.infoDict = {}
		self.filename = ""
		QtCore.QObject.connect(self.web.page().networkAccessManager(), QtCore.SIGNAL("finished(QNetworkReply *)"), self.replyFinished)
		QtCore.QObject.connect(self.web.page().mainFrame(), QtCore.SIGNAL("loadFinished(bool)"), self.loadFinished)
		self.web.load(QtCore.QUrl("https://matkatv.dna.fi/webui/site/login"))

	def keyPressEvent(self, event):
		if event.key() == QtCore.Qt.Key_Control:
			self.dl = not self.dl
			if self.dl:
				self.setWindowTitle("MatkaTeevee - Lataus")
			else:
				self.setWindowTitle("MatkaTeevee - Toisto")
		if event.key() == QtCore.Qt.Key_Escape:
			self.close()

	def replyFinished(self, reply):
		url = unicode(reply.url().toString())

		if url.find("mp4?") != -1:
			docE = self.web.page().mainFrame().documentElement()
			closeE = docE.findFirst("[title=close]")
			closeE.evaluateJavaScript("this.click()")

			if url.find("jwplayer") == -1:
				mp4url = url[:url.rfind("&")]

				if self.dl:
					Id=mp4url[mp4url.rfind("/"):mp4url.find("-1024k")]
					self.infoDict["ID"]=Id[Id.find("-")+1:]
					self.infoCollector()
					q=""
					if wget.find("'") != -1 :
						q="'"
					fOut = unicode(" -O " + chr(34) + self.filename + chr(34) + ".mp4" + q)
					subprocess.Popen(wget + chr(34) + mp4url + chr(34) + fOut, shell=True)
				else:
					subprocess.Popen(vlc + chr(34) +  mp4url + chr(34), shell=True)
			

		if url.find("/logout") != -1:
			self.close()

	def infoCollector(self):
		docE = self.web.page().mainFrame().documentElement()
		idStr = self.infoDict["ID"]
		infoE = docE.findFirst("""[href="#/recordings/""" + idStr + chr(34) + "]")
		infoPE = infoE.parent().parent()
		title = unicode(infoPE.findFirst("[class=programme-title]").toPlainText())
		title = re.sub("""[<>"/\|?*]""","", title)
		self.infoDict["Ohjelma"] = title.replace(":",",")
		aika = infoPE.findFirst("[class=programme-date]").toPlainText() + " " + infoPE.findFirst("[class=programme-time]").toPlainText()
		self.infoDict["Aika"] = aika[aika.indexOf(",")+2:aika.lastIndexOf(":")].replace(":","")
		self.infoDict["Kuvaus"] = infoPE.findFirst("[class=description]").toPlainText().replace(chr(92), "")

		#Tiedostonimen muoto
		filename = directory + self.infoDict["Ohjelma"] + "(" + self.infoDict["Aika"] + ")"
		self.filename = unicode(filename)
		file = open(self.filename + ".txt", 'w')

		#Info tiedoston sis�lt�
		info = "Ohjelma: " +  self.infoDict["Ohjelma"] +"\nAika: "+ self.infoDict["Aika"] + "\n\nKuvaus: " + self.infoDict["Kuvaus"]

		file.write(info)
		file.close()

	def loadFinished(self, ok):
		url1 = unicode(self.web.url().toString())

		if url1.find("https://matkatv.dna.fi/webui/site/login", 0) != -1 and uname!="":
			jsLogin="""
			document.getElementById('LoginForm_username').value=""" + chr(34) + uname+ chr(34) + """;
			document.getElementById('LoginForm_password').value=""" + chr(34) + passw+ chr(34) + """;
			document.getElementById('login-btn').click();
			"""
			self.web.page().mainFrame().evaluateJavaScript(jsLogin)

		if url1.find("https://matkatv.dna.fi/webui?") != -1 or url1.find("https://matkatv.dna.fi/webui") != -1 		and len(url1)<=28:
			self.web.load(QtCore.QUrl("https://matkatv.dna.fi/webui/myrecordings#recorded"))

app = QtGui.QApplication(sys.argv)
MatkaTeeve = MatkaTeeve()
MatkaTeeve.show()
sys.exit(app.exec_())
